$('#slide-banner').owlCarousel({
    loop:true,
    margin:0,
    nav:true,
    items:1,
    animateOut: 'fadeOut',
    autoplay:true,
    autoplayTimeout:5000,
})
$('#slide-trademark').owlCarousel({
    loop:true,
    margin:20,
    nav:true,
    items:6,
    animateOut: 'fadeOut',
    autoplay:true,
    autoplayTimeout:10000,
    responsive : {
        0:{
            items:3
        },
        480 : {
            items:3
        },
        768 : {
            items:4
        }
    }
})
if ($('#imageGallery')) {
    var slider_detail = $('#imageGallery').lightSlider({
        gallery: true,
        autoWidth: false,
        adaptiveHeight:false,
        item: 1,
        loop:true,
        slideMargin: 0,
        thumbItem:5,
        auto:true,
        speed:1000,
        responsive : [
            {
                breakpoint:480,
                settings: {
                    thumbItem:3
                }
            }
        ]
    }); 
}
$('.slideControls .slidePrev').click(function() {
    slider_detail.goToPrevSlide();
});

$('.slideControls .slideNext').click(function() {
    slider_detail.goToNextSlide();
});
// slider FUTURE PRICE
var input_min = $("#input_min");
var input_max = $("#input_max");
var slider = $('#slider');
var min_range = parseInt(slider.attr('attr-min'));
var max_range = parseInt(slider.attr('attr-max'));
function slider_input(input_min,input_max) {
    $('#slider').slider({
        range: true,
        min: min_range,
        max: max_range,
        values: [ input_min, input_max ],
        slide: function(event, ui) {
            if ( ui.values[0] == ui.values[1] ) {
                $('.price-range-both i').css('display', 'none');
            } else {
                $('.price-range-both i').css('display', 'inline');
            }
      }
    });
}
input_min.keyup(function(){
    var number_min = $(this).val();
    var number_max = input_max.val();
    slider_input(number_min,number_max);
});
input_max.keyup(function(){
    var number_max = $(this).val();
    var number_min = input_min.val();
    slider_input(number_min,number_max);
});
// $("#iput").val()
// slider.attr('attr-max')
slider.slider({
    range: true,
    min: min_range,
    max: max_range,
    values: [ input_min.val(), input_max.val() ],
    slide: function(event, ui) {
        input_min.val(ui.values[ 0 ])
        input_max.val(ui.values[ 1 ])
        if ( ui.values[0] == ui.values[1] ) {
            $('.price-range-both i').css('display', 'none');
        } else {
            $('.price-range-both i').css('display', 'inline');
        }
    }
});
$('.ui-slider-range').append('<span class="price-range-both value"></span>');
// $' + $('#slider').slider('values', 0 ) + ' - </i>' + $('#slider').slider('values', 1 ) + '
$('.ui-slider-handle:eq(0)').append('<span class="price-range-min value"></span>');
// $' + $('#slider').slider('values', 0 ) + '
$('.ui-slider-handle:eq(1)').append('<span class="price-range-max value"></span>');
// $' + $('#slider').slider('values', 1 ) + '
input_min.val(slider.slider('values', 0 ))
input_max.val(slider.slider('values', 1 ))


$(document).ready(function() {
});
$(window).scroll(function(){
    if ($(window).scrollTop() >= 200) {
        $('.menu').addClass('fixed-header');
        $('#logo-scoll').css({"display":"block"});
        $('#logo-no-scoll').css({"display":"none"});
        $('nav div').addClass('visible-title');
    }
    else {
        $('.menu').removeClass('fixed-header');
        $('#logo-scoll').css({"display":"none"});
        $('#logo-no-scoll').css({"display":"block"});
        $('nav div').removeClass('visible-title');
    }
    if ($(this).scrollTop() >= 50) {
        $('#return-to-top').fadeIn(200);
    } else {
        $('#return-to-top').fadeOut(200);
    }
});
// $('#return-to-top').click(function() {
//     $('body,html').animate({
//         scrollTop : 0
//     }, 500);
// });